package ru.kpfu.itis;

import ru.kpfu.itis.entity.*;

import java.util.*;

/**
 * Created by Рустем on 01.04.2015.
 */
public class Fixture {

    public static final String USER_NAME = "blablabla";
    public static final String USER_EMAIL = "blablabla@w.ru";
    public static final String USER_PASS = "123123";
    private static final String TWEET_TEXT = "TEXTTEXT";
    private static final String TWEET_COORDS = "0";
    private static final Integer TWEET_ID = 1;
    private static final Integer ROLE_ID = 1;
    private static final String ROLE_NAME = "ROLE_USER";
    public static Boolean USER_ENABLED = true;
    public static Integer USER_ID = 1;
    public static final String STATUS_NAME = "STATUS_OPEN";
    public static final Integer STATUS_ID = 1;
    public static final Integer RETWEET_ID = 2;

    public static Status standardStatus(){
        Status standartStatus = new Status();
        standartStatus.setName(STATUS_NAME);
        standartStatus.setId(STATUS_ID);
        return standartStatus;
    }


    public static User standardUser() {
        User user = new User();
        user.setId(USER_ID);
        user.setEmail(USER_EMAIL);
        user.setName(USER_NAME);
        user.setPassword(USER_PASS);
        user.setEnabled(USER_ENABLED);
        user.setTweets(standartTweets());
        user.setAccessibleTweets(standartAccTweets());
        user.setRoles(standartRoles());
        return user;
    }

    public static Tweet standartTweet() {
        Tweet tweet = new Tweet();
        tweet.setId(TWEET_ID);
        tweet.setCoords(TWEET_COORDS);
        tweet.setPublishedDate(new Date());
        tweet.setText(TWEET_TEXT);
        return tweet;
    }

    public static ReTweet standartReTweet() {
        ReTweet reTweet = new ReTweet();
        reTweet.setId(RETWEET_ID);
        reTweet.setPublishedDate(new Date());
        reTweet.setText(TWEET_TEXT);
        reTweet.setTweet(standartTweet());
        return reTweet;
    }

    public static Role standartRole() {
        Role role = new Role();
        role.setId(ROLE_ID);
        role.setName(ROLE_NAME);
        List<User> userList = new ArrayList<User>();
        userList.add(new User());
        role.setUsers(userList);
        return role;
    }

    public static Set<Tweet> standartAccTweets() {
        Set<Tweet> tweets = new HashSet<>();
        Tweet tweet = new Tweet();
        tweet.setId(TWEET_ID);
        tweet.setCoords(TWEET_COORDS);
        tweet.setPublishedDate(new Date());
        tweet.setText(TWEET_TEXT);
        tweets.add(tweet);
        tweets.add(tweet);
        tweets.add(tweet);
        return tweets;
    }

    public static List<Tweet> standartTweets() {
        List<Tweet> tweets = new ArrayList<Tweet>();
        tweets.add(standartTweet());
        tweets.add(standartTweet());
        tweets.add(standartTweet());
        return tweets;
    }

    public static List<User> standartUsers() {
        List<User> users = new ArrayList<User>();
        users.add(standardUser());
        users.add(standardUser());
        users.add(standardUser());
        return users;
    }

    public static List<Role> standartRoles() {
        List<Role> roles = new ArrayList<Role>();
        roles.add(standartRole());
        return roles;
    }

    public static User stUserWithRoles() {
        User us = standardUser();
        us.setRoles(standartRoles());
        return us;
    }
}
