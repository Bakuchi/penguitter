package ru.kpfu.itis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.repository.ReTweetRepository;
import ru.kpfu.itis.repository.TweetRepository;
import ru.kpfu.itis.service.ReTweetService;
import ru.kpfu.itis.service.TweetService;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.kpfu.itis.Fixture.standartReTweet;
import static ru.kpfu.itis.Fixture.standartTweet;

/**
 * Created by User on 12.05.2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class ReTweetServiceTests {

    @Spy
    @InjectMocks
    ReTweetService service;

    @Mock
    ReTweetRepository reTweetRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUserService_deleteTweet() throws Exception {
        ReTweet rt = standartReTweet();
        service.delete(rt);
        verify(reTweetRepository, times(1)).delete(rt);
    }

    @Test
    public void testUserService_findOne() throws Exception {
        when(reTweetRepository.findOne(1)).thenReturn(standartReTweet());
        ReTweet expected = standartReTweet();

        ReTweet actual = service.findOne(1);

        assertEquals(expected.getId(), actual.getId());
    }

    @Test
    public void testTweetService_save() throws Exception {
        ReTweet testReTweet = standartReTweet();
        service.save(testReTweet);
        verify(reTweetRepository, times(1)).save(testReTweet);
    }
}

