package ru.kpfu.itis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.repository.RoleRepository;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.UserService;

import java.util.*;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static ru.kpfu.itis.Fixture.*;

/**
 * Created by Рустем on 01.04.2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    @Spy
    @InjectMocks
    UserService service;

    @Mock
    UserRepository userRepository;

    @Mock
    RoleRepository roleRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUserService_deleteUser() throws Exception {
        when(userRepository.findOne(1)).thenReturn(standardUser());
        service.delete(1);
        verify(userRepository, times(1)).delete(1);
    }

    @Test
    public void testUserService_update() throws Exception {
        User testUser = new User();
        when(userRepository.findOne(testUser.getId())).thenReturn(testUser);
        service.update(testUser);
        verify(userRepository, times(1)).save(testUser);
    }

    @Test
    public void testUserService_findOneByName() throws Exception {
        when(userRepository.findByName("blablabla")).thenReturn(standardUser());

        User expected = standardUser();

        User actual = service.findOneByName("blablabla");

        assertEquals(expected.getName(), actual.getName());
    }

    @Test
    public void testUserService_findOneById() throws Exception {
        when(userRepository.findOne(1)).thenReturn(standardUser());

        User expected = standardUser();

        User actual = service.findOneByID(1);

        assertEquals(expected.getName(), actual.getName());
    }

    @Test
    public void testUserService_findAll() throws Exception {
        when(userRepository.findAll()).thenReturn(standartUsers());
        List<User> expected = standartUsers();
        List<User> actual = userRepository.findAll();
        assertEquals(actual, expected);
    }

    @Test
    public void testUserService_save() throws Exception {
        User testUser = standardUser();
        service.save(testUser);
        verify(userRepository, times(1)).save(testUser);
    }
}
