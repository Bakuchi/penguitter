package ru.kpfu.itis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.repository.RoleRepository;
import ru.kpfu.itis.repository.TweetRepository;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.kpfu.itis.Fixture.*;

/**
 * Created by User on 12.05.2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class TweetServiceTests {

    @Spy
    @InjectMocks
    TweetService service;

    @Mock
    TweetRepository tweetRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUserService_deleteTweet() throws Exception {
        Tweet st = standartTweet();
        service.delete(st);
        verify(tweetRepository, times(1)).delete(st);
    }

    @Test
    public void testUserService_findOne() throws Exception {
        when(tweetRepository.findOne(1)).thenReturn(standartTweet());
        Tweet expected = standartTweet();

        Tweet actual = service.findOne(1);

        assertEquals(expected.getId(), actual.getId());
    }

    @Test
    public void testTweetService_save() throws Exception {
        Tweet testTweet = standartTweet();
        service.save(testTweet);
        verify(tweetRepository, times(1)).save(testTweet);
    }
}
