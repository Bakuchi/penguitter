package ru.kpfu.itis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import ru.kpfu.itis.entity.Status;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.repository.StatusRepository;
import ru.kpfu.itis.service.StatusService;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static ru.kpfu.itis.Fixture.standardStatus;
import static ru.kpfu.itis.Fixture.standardUser;

/**
 * Created by User on 11.05.2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class StatusServiceTests {

    @Spy
    @InjectMocks
    StatusService service;

    @Mock
    StatusRepository statusRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testStatusService_findOneByName() throws Exception {
        when(statusRepository.findByName("STATUS_OPEN")).thenReturn(standardStatus());
        Status expected = standardStatus();
        Status actual = service.findOneByName("STATUS_OPEN");
        assertEquals(expected.getName(), actual.getName());
    }


}
