package ru.kpfu.itis.batch;

import org.springframework.batch.item.ItemProcessor;
import ru.kpfu.itis.entity.ReTweet;

/**
 * Created by ������ on 05.05.2015.
 */
public class ReTweetProcessor implements ItemProcessor<ReTweet, ReTweet> {
    @Override
    public ReTweet process(ReTweet reTweet) throws Exception {
        Integer days_ago = reTweet.getDays_ago();
        System.out.println("before" + days_ago);
        days_ago++;
        System.out.println("after" + days_ago);
        reTweet.setDays_ago(days_ago);
        return reTweet;
    }
}