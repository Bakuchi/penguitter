package ru.kpfu.itis.batch;

import org.springframework.jdbc.core.RowMapper;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ������ on 06.05.2015.
 */
public class TweetRowMapper implements RowMapper<Tweet> {
    @Override
    public Tweet mapRow(ResultSet resultSet, int i) throws SQLException {
        Tweet tweet = new Tweet();

        tweet.setId(resultSet.getInt("id"));
//        tweet.setText(resultSet.getString("text"));
        tweet.setDays_ago(resultSet.getInt("days_ago"));
//        ID, COORDS,PUBLISHED_DATE,  TEXT, STATUS_ID, USER_ID, DAYS_AGO

        return tweet;
    }
}
