package ru.kpfu.itis.batch;

import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.service.ReTweetService;

import java.util.List;

/**
 * Created by ������ on 05.05.2015.
 */
public class ReTweetWriter extends JpaItemWriter<ReTweet> {
    @Autowired
    ReTweetService reTweetService;

    @Override
    public void write(List<? extends ReTweet> list) {
        for (ReTweet reTweet : list) {
            ReTweet rt = reTweetService.findOne(reTweet.getId());
            rt.setDays_ago(reTweet.getDays_ago());
            reTweetService.save(rt);
        }
    }
}
