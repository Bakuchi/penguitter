package ru.kpfu.itis.batch;

import org.springframework.jdbc.core.RowMapper;
import ru.kpfu.itis.entity.ReTweet;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ������ on 06.05.2015.
 */
public class ReTweetRowMapper implements RowMapper<ReTweet> {
    @Override
    public ReTweet mapRow(ResultSet resultSet, int i) throws SQLException {
        ReTweet reTweet = new ReTweet();

        reTweet.setId(resultSet.getInt("id"));
//        tweet.setText(resultSet.getString("text"));
        reTweet.setDays_ago(resultSet.getInt("days_ago"));
//        ID, COORDS,PUBLISHED_DATE,  TEXT, STATUS_ID, USER_ID, DAYS_AGO

        return reTweet;
    }
}
