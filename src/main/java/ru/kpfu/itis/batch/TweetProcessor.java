package ru.kpfu.itis.batch;

import org.springframework.batch.item.ItemProcessor;
import ru.kpfu.itis.entity.Tweet;

/**
 * Created by ������ on 05.05.2015.
 */
public class TweetProcessor implements ItemProcessor<Tweet, Tweet> {
    @Override
    public Tweet process(Tweet tweet) throws Exception {
        Integer days_ago = tweet.getDays_ago();
        System.out.println("before" + days_ago);
        days_ago++;
        System.out.println("after" + days_ago);
        tweet.setDays_ago(days_ago);
        return tweet;
    }
}