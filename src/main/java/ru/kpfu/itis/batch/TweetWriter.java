package ru.kpfu.itis.batch;

import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.service.TweetService;

import java.util.List;

/**
 * Created by ������ on 05.05.2015.
 */
public class TweetWriter extends JpaItemWriter<Tweet> {
    @Autowired
    TweetService tweetService;

    @Override
    public void write(List<? extends Tweet> list) {
        for (Tweet tweet : list) {
            Tweet tw = tweetService.findOne(tweet.getId());
            tw.setDays_ago(tweet.getDays_ago());
            tweetService.save(tw);
        }
    }
}
