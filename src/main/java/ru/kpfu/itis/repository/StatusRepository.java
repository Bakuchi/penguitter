package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.entity.Status;

public interface StatusRepository extends JpaRepository<Status, Integer> {

    Status findByName(String name);

}
