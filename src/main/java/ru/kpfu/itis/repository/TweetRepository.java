package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.entity.User;

import java.util.List;

public interface TweetRepository extends JpaRepository<Tweet, Integer>{

	List<Tweet> findByUser(User user);
}
