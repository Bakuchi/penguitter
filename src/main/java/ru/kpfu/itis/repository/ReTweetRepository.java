package ru.kpfu.itis.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.entity.Tweet;

import java.util.List;

public interface ReTweetRepository extends JpaRepository<ReTweet, Integer> {

	List<ReTweet> findByTweet(Tweet tweet);
}
