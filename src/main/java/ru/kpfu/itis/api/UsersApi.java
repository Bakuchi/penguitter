package ru.kpfu.itis.api;

import org.hibernate.annotations.SourceType;
import org.hibernate.internal.IteratorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.entity.Profile;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.service.ReTweetService;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Рустем on 13.04.2015.
 */
@RestController
@RequestMapping("/")
public class UsersApi {

    @Autowired
    private UserService userService;

    @Autowired
    private TweetService tweetService;

    @Autowired
    private ReTweetService reTweetService;

    @RequestMapping(value = "users/{name}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<User> getOne(@PathVariable String name) {
        User user = userService.findOneByName(name);
        if (user != null) {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "tweets/add/{id}", method = RequestMethod.POST, consumes = {"application/json"})
    public
    @ResponseBody
    void addToList(@PathVariable Integer id, @RequestBody Profile person) {
        User user = userService.findOneByNameTweet(person.getUser_name());
        Set<Tweet> userAccessibleTweets = user.getAccessibleTweets();
        Tweet tweet = tweetService.findOneTweet(id);
        userAccessibleTweets.add(tweet);
        user.setAccessibleTweets(userAccessibleTweets);
        userService.update(user);
    }

    @RequestMapping(value = "tweets/delete/{id}", method = RequestMethod.POST, consumes = {"application/json"})
    public
    @ResponseBody
    void removeFromList(@PathVariable Integer id, @RequestBody Profile person) {
        User user = userService.findOneByNameTweet(person.getUser_name());
        Set<Tweet> userAccessibleTweets = user.getAccessibleTweets();
        Tweet tweet = tweetService.findOneTweet(id);
        userAccessibleTweets.remove(tweet);
        for (Tweet t  : userAccessibleTweets) {
            if(t.getId() == tweet.getId())
                userAccessibleTweets.remove(t);
        }
        user.setAccessibleTweets(userAccessibleTweets);
        userService.update(user);
    }


    @RequestMapping(value = "retweets/add/{id}", method = RequestMethod.POST, consumes = {"application/json"})
    public
    @ResponseBody
    void addToListre(@PathVariable Integer id, @RequestBody Profile person) {
        User user = userService.findOneByNameReTweet(person.getUser_name());
        Set<ReTweet> userAccessibleTweets = user.getAccessiblereTweets();
        ReTweet reTweet =  reTweetService.findOneTw(id);
        userAccessibleTweets.add(reTweet);
        user.setAccessiblereTweets(userAccessibleTweets);
        userService.update(user);
    }

    @RequestMapping(value = "retweets/delete/{id}", method = RequestMethod.POST, consumes = {"application/json"})
    public
    @ResponseBody
    void removeFromListre(@PathVariable Integer id, @RequestBody Profile person) {
        User user = userService.findOneByNameReTweet(person.getUser_name());
        Set<ReTweet> userAccessibleTweets = user.getAccessiblereTweets();
        ReTweet reTweet = reTweetService.findOneTw(id);
        for (ReTweet t  : userAccessibleTweets) {
            if(t.getId() == reTweet.getId())
                userAccessibleTweets.remove(t);
        }
        user.setAccessiblereTweets(userAccessibleTweets);
        userService.update(user);
    }
}
