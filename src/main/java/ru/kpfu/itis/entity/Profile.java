package ru.kpfu.itis.entity;

import java.io.Serializable;

/**
 * Created by Рустем on 12.04.2015.
 */
public class Profile implements Serializable{

    private String user_name;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
