package ru.kpfu.itis.entity;

/**
 * Created by User on 12.04.2015.
 */
public class UserRetweet {
    private User user;
    private ReTweet reTweet;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ReTweet getReTweet() {
        return reTweet;
    }

    public void setReTweet(ReTweet reTweet) {
        this.reTweet = reTweet;
    }
}
