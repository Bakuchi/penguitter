package ru.kpfu.itis.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class Tweet {

	@Id
	@GeneratedValue
	private Integer id;
	
	@Size(min = 1, max=150, message = "Invalid Size!")
	@Column(length = 1000)
	private String text;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "app_user_tweet",
            joinColumns = {@JoinColumn(name = "app_user_id")},
            inverseJoinColumns = {@JoinColumn(name = "accessibletweets_id")})
    private Set<User> usersWithAccess;

    @Column(name = "published_date")
    private Date publishedDate;

	@Column(name="coords")
	private String coords;

    @ManyToOne
    @JoinColumn(name="status_id")
    private Status status;

	@OneToMany(mappedBy = "tweet", cascade = CascadeType.REMOVE)
	private List<ReTweet> reTweets;

    @Column
	private Integer days_ago = 0;

    public Integer getDays_ago() {
        return days_ago;
    }

    public void setDays_ago(Integer days_ago) {
        this.days_ago = days_ago;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ReTweet> getReTweets() {
		return reTweets;
	}

	public void setReTweets(List<ReTweet> reTweets) {
		this.reTweets = reTweets;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCoords() {
		return coords;
	}

	public void setCoords(String coords) {
		this.coords = coords;
	}

    public String getPublishedDate() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
        return simpleDateFormat.format(publishedDate.getTime()-3600000);
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<User> getUsersWithAccess() {
        return usersWithAccess;
    }

    public void setUsersWithAccess(Set<User> usersWithAccess) {
        this.usersWithAccess = usersWithAccess;
    }
}
