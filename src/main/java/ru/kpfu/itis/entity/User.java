package ru.kpfu.itis.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import ru.kpfu.itis.annotation.UniqueUsername;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "app_user")
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    @Pattern(regexp = "^[a-zA-Z0-9_]+$", message = "������������ �����!!!")
    @Size(min = 3, message = "Name must be at least 3 characters!")
    @Column(unique = true)
    @UniqueUsername(message = "Such username already exists!")
    private String name;

    @Size(min = 1, message = "Invalid email address!")
    @Email(message = "Invalid email address!")
    private String email;

    @JsonIgnore
    @Size(min = 5, message = "Name must be at least 5 characters!")
    private String password;

    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable
    @JsonIgnore
    private List<Role> roles;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<Tweet> tweets;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<ReTweet> reTweets;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "app_user_tweet",
            joinColumns = {@JoinColumn(name = "accessibletweets_id")},
            inverseJoinColumns = {@JoinColumn(name = "app_user_id")})
    private Set<Tweet> accessibleTweets;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "app_user_retweet",
            joinColumns = {@JoinColumn(name = "accessibleretweets_id")},
            inverseJoinColumns = {@JoinColumn(name = "app_user_id")})
    private Set<ReTweet> accessiblereTweets;

    @JsonIgnore
    public Set<ReTweet> getAccessiblereTweets() {
        return accessiblereTweets;
    }

    public void setAccessiblereTweets(Set<ReTweet> accessiblereTweets) {
        this.accessiblereTweets = accessiblereTweets;
    }

    @JsonIgnore
    public Set<Tweet> getAccessibleTweets() {
        return accessibleTweets;
    }

    public void setAccessibleTweets(Set<Tweet> accessibleTweets) {
        this.accessibleTweets = accessibleTweets;
    }

    @JsonIgnore
    public List<ReTweet> getReTweets() {
        return reTweets;
    }

    public void setReTweets(List<ReTweet> reTweets) {
        this.reTweets = reTweets;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean getEnabled(){
        return enabled;
    }

    @JsonIgnore
    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    @JsonIgnore
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {

        User us = (User) obj;
        if (this.getName().equals(us.getName()))
            return true;
        else
            return false;
    }
}
