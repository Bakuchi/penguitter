package ru.kpfu.itis.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String name;

    @OneToMany(mappedBy = "status", cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private List<Tweet> tweets;

    @OneToMany(mappedBy = "status", cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private List<ReTweet> retweets;

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ReTweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(List<ReTweet> retweets) {
        this.retweets = retweets;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }
}
