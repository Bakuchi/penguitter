package ru.kpfu.itis.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.repository.TweetRepository;
import ru.kpfu.itis.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class TweetService {

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private UserRepository userRepository;

    public void save(Tweet tweet) {
        tweetRepository.save(tweet);
    }

    @PreAuthorize("#tweet.user.name == authentication.name or hasRole('ROLE_ADMIN')")
    public void delete(@P("tweet") Tweet tweet) {
        if (tweet!=null)
            tweetRepository.delete(tweet);
    }

    @Transactional
    public Tweet findOneTweet(int id) {
        Tweet tweet = findOne(id);
        Hibernate.initialize(tweet.getUsersWithAccess());
        return tweet;
    }

    public Tweet findOne(int id) {
        Tweet tweet = tweetRepository.findOne(id);
        return tweet;
    }

    public List<Tweet> findAll() {
        return tweetRepository.findAll();
    }

    public List<Tweet> findTop10() {
        List<Tweet> tweets = tweetRepository.findAll();
        List<Tweet> tweetList = new ArrayList<>();
        if (tweets.size() - 1 >= 10) {
            for (int i = 1; i <= 10; i++) {
                tweetList.add(i - 1, tweets.get(tweets.size() - i));
            }
        } else {
            for (int i = 1; i <= tweets.size() - 1; i++) {
                tweetList.add(i - 1, tweets.get(tweets.size() - i));
            }
        }
        return tweetList;
    }

}
