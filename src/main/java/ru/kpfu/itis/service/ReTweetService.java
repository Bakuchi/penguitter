package ru.kpfu.itis.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.repository.ReTweetRepository;
import ru.kpfu.itis.repository.UserRepository;

import javax.transaction.Transactional;

@Service
public class ReTweetService {

    @Autowired
    private ReTweetRepository reTweetRepository;

    @Autowired
    private UserRepository userRepository;

    public void save(ReTweet retweet) {
        reTweetRepository.save(retweet);
    }

    @PreAuthorize("#retweet.user.name == authentication.name or hasRole('ROLE_ADMIN')")
    public void delete(@P("retweet") ReTweet retweet) {
        if (retweet!=null)
            reTweetRepository.delete(retweet);
    }

    public ReTweet findOne(int id) {
        return reTweetRepository.findOne(id);
    }

    @Transactional
    public ReTweet findOneTw(int id ){
        ReTweet rt = findOne(id);
        Hibernate.initialize(rt.getUsersWithAccess());
        return rt;
    }
}
