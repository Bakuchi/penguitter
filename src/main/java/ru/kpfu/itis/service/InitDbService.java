package ru.kpfu.itis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.entity.Role;
import ru.kpfu.itis.entity.Status;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.repository.*;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class InitDbService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private ReTweetRepository reTweetRepository;

    @Autowired
    private StatusRepository statusRepository;

    @PostConstruct
    public void init() {
        if (statusRepository.findByName("STATUS_OPEN") == null) {

        Status statusOpen = new Status();
        statusOpen.setName("STATUS_OPEN");
        statusRepository.save(statusOpen);

        Status statusClosed = new Status();
        statusClosed.setName("STATUS_CLOSED");
        statusRepository.save(statusClosed);

        Status statusSecret = new Status();
        statusSecret.setName("STATUS_SECRET");
        statusRepository.save(statusSecret);

        }

        if (roleRepository.findByName("ROLE_ADMIN") == null) {
            Role roleUser = new Role();
            roleUser.setName("ROLE_USER");
            roleRepository.save(roleUser);

            Role roleAdmin = new Role();
            roleAdmin.setName("ROLE_ADMIN");
            roleRepository.save(roleAdmin);

            User userAdmin = new User();
            userAdmin.setEnabled(true);
            userAdmin.setName("admin");
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            userAdmin.setPassword(encoder.encode("admin"));
            List<Role> roles = new ArrayList<Role>();
            roles.add(roleAdmin);
            roles.add(roleUser);
            userAdmin.setRoles(roles);
            userRepository.save(userAdmin);
        }

    }
}
