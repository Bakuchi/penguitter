package ru.kpfu.itis.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.entity.Role;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.repository.RoleRepository;
import ru.kpfu.itis.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOneByID(int id) {
        return userRepository.findOne(id);
    }

    public User findOneByName(String name) {
        return userRepository.findByName(name);
    }

    @Transactional
    public User findOneByNameTweet(String name) {
        User us = findOneByName(name);
        Hibernate.initialize(us.getAccessibleTweets());
        return us;
    }

    @Transactional
    public User findOneByNameReTweet(String name) {
        User us = findOneByName(name);
        Hibernate.initialize(us.getAccessiblereTweets());
        return us;
    }

    public void save(User user) {
        if (user != null) {
            user.setEnabled(true);
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            user.setPassword(encoder.encode(user.getPassword()));

            List<Role> roles = new ArrayList<Role>();
            roles.add(roleRepository.findByName("ROLE_USER"));
            user.setRoles(roles);

            userRepository.save(user);
        }
    }

    public void update(User user) {
        if (userRepository.findOne(user.getId()) != null)
            userRepository.save(user);
    }

    public void delete(int id) {
        if (userRepository.findOne(id) != null) {
            userRepository.delete(id);
        }
    }
}
