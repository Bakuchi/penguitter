package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.entity.Status;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.service.ReTweetService;
import ru.kpfu.itis.service.StatusService;

import java.util.HashSet;

/**
 * Created by ������ on 26.04.2015.
 */
@Controller
@RequestMapping("/retweet/{id}")
public class ReTweetController extends BaseController {

    @Autowired
    private StatusService statusService;

    @Autowired
    private ReTweetService reTweetService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewTweet(@PathVariable Integer id) {
        ReTweet rt = reTweetService.findOneTw(id);
        request.setAttribute("tweet", rt);
        return "view_rt";
    }

    //    edit access for retweet
    @RequestMapping(value = "/edit_access", method = RequestMethod.GET)
    public String tweet(@PathVariable int id) {
        request.setAttribute("retweet", reTweetService.findOneTw(id));
        return "edit_access";
    }

    //    Edit details for retweet
    @RequestMapping(value = "/edit_retweet", method = RequestMethod.GET)
    public String edit_retweet(@PathVariable int id) {
        request.setAttribute("retweet", reTweetService.findOneTw(id));
        return "edit_retweet";
    }

    @RequestMapping(value = "/edit_retweet", method = RequestMethod.POST)
    public String editreTweet(@PathVariable int id, @RequestParam String text, @RequestParam String status) {
        ReTweet rt = reTweetService.findOne(id);
        rt.setText(text);
        Status st = statusService.findOneByName(status);
        rt.setStatus(st);
        if (st.getId() == 2) {
            reTweetService.save(rt);
            return tweet(id);
        } else {
            rt.setUsersWithAccess(new HashSet<>());
            reTweetService.save(rt);
            return "redirect:/profile/"+rt.getUser().getName()+".html";
        }
    }
}
