package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.service.TweetService;

@Controller
public class IndexController extends BaseController{

    @Autowired
    private TweetService tweetService;

	@RequestMapping(value = "/index")
	public String index() {
        request.setAttribute("tweets", tweetService.findTop10());
		return "index";
	}

}