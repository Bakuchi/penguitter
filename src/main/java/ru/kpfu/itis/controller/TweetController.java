package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.entity.Status;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.repository.ReTweetRepository;
import ru.kpfu.itis.service.ReTweetService;
import ru.kpfu.itis.service.StatusService;
import ru.kpfu.itis.service.TweetService;

import java.util.HashSet;

/**
 * Created by ������ on 26.04.2015.
 */
@Controller
@RequestMapping("/tweet/{id}")
public class TweetController extends BaseController {

    @Autowired
    private TweetService tweetService;
    @Autowired
    private StatusService statusService;

    @Autowired
    ReTweetRepository reTweetRepository;

    //    View tweet
    @RequestMapping(method = RequestMethod.GET)
    public String viewTweet(@PathVariable Integer id) {
        Tweet tw = tweetService.findOneTweet(id);
        request.setAttribute("tweet", tw);
        return "view";
    }

    //    Edit access for tweet open up
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String tweet(@PathVariable int id) {
        request.setAttribute("tweet", tweetService.findOneTweet(id));
        return "edit";
    }

    //    Edit details for tweet
    @RequestMapping(value = "/edit_tweet", method = RequestMethod.GET)
    public String edit_tweet(@PathVariable int id) {
        request.setAttribute("tweet", tweetService.findOneTweet(id));
        return "edit_tweet";
    }


    @RequestMapping(value = "/edit_tweet", method = RequestMethod.POST)
    public String editTweet(@PathVariable int id, @RequestParam String text, @RequestParam String coords, @RequestParam String status) {
        Tweet tw = tweetService.findOneTweet(id);
        tw.setText(text);
        tw.setCoords(coords);
        Status st = statusService.findOneByName(status);
        tw.setStatus(st);
        if (st.getId() == 2) {
            tweetService.save(tw);
            return tweet(id);
        } else {
            tw.setUsersWithAccess(new HashSet<>());
            tweetService.save(tw);
            return "redirect:/profile/"+tw.getUser().getName()+".html";
        }
    }
}
