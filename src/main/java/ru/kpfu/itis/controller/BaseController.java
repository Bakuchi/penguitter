package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 12.04.2015.
 */
@Controller
public class BaseController {
    @Autowired
    protected HttpServletRequest request;


    public static String redirectToMain(){return "redirect:/";}
}
