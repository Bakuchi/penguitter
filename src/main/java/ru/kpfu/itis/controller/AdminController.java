package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.Mailing.Mailing;
import ru.kpfu.itis.entity.User;
import ru.kpfu.itis.service.ReTweetService;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;

@Controller
@RequestMapping("/users")
public class AdminController {

	@Autowired
	private UserService userService;

	@Autowired
	private TweetService tweetService;

	@Autowired
	private ReTweetService reTweetService;

	@RequestMapping
	public String users(Model model) {
		model.addAttribute("users", userService.findAll());
		return "users";
	}

	@RequestMapping("/{id}")
	public String detail(Model model, @PathVariable int id) {
		model.addAttribute("user", userService.findOneByID(id));
		return "user-detail";
	}

	@RequestMapping("/remove/{id}")
	public String removeUser(@PathVariable int id) {
		userService.delete(id);
		return "redirect:/users.html";
	}

	@RequestMapping("/{id}/edit")
	public String enabledUser(@PathVariable int id) {
		boolean f = userService.findOneByID(id).getEnabled();
		User user = userService.findOneByID(id);
		Mailing mailing = new Mailing();
		if (user.getEnabled()){
			mailing.sendMail("digitalzonelaboratory@gmail.com", user.getEmail(), "��� ����!", "�� �������� ��� �� ����������!");
		}
		else {
			mailing.sendMail("digitalzonelaboratory@gmail.com", user.getEmail(), "��� ��������������!", "��������� ���� ��� ��� ���� ����.");
		}
		user.setEnabled(!f);
		userService.update(user);
		return "redirect:/users.html";
	}
}
