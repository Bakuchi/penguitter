package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.entity.ReTweet;
import ru.kpfu.itis.entity.Status;
import ru.kpfu.itis.entity.Tweet;
import ru.kpfu.itis.service.ReTweetService;
import ru.kpfu.itis.service.StatusService;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;

import java.security.Principal;
import java.util.Date;

/**
 * Created by Roman on 12.04.2015.
 */
@Controller
@RequestMapping("/profile/{username}")
public class ProfileController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private TweetService tweetService;

    @Autowired
    private ReTweetService reTweetService;

    @Autowired
    private StatusService statusService;

    @RequestMapping(method = RequestMethod.GET, params = {"principal"})
    public String viewAccount(@PathVariable String username, Principal principal) {
        request.setAttribute("req", userService.findOneByName(principal.getName()));
        request.setAttribute("user", userService.findOneByNameTweet(username));
        return "profile";
    }

    @RequestMapping(method = RequestMethod.GET, params = {"!principal"})
    public String viewAccount(@PathVariable String username) {
        request.setAttribute("user", userService.findOneByNameTweet(username));
        return "profile";
    }

    @RequestMapping(method = RequestMethod.POST, params = {"tweetID"})
    public String addReTweet(@RequestParam String text, @RequestParam String tweetID,
                             Principal principal) {
        ReTweet reTweet = new ReTweet();
        reTweet.setUser(userService.findOneByName(principal.getName()));
        reTweet.setText(text);
        reTweet.setTweet(tweetService.findOne(Integer.parseInt(tweetID)));
        Status st = new Status();
        st.setId(1);
        reTweet.setStatus(st);
        reTweet.setPublishedDate(new Date());
        reTweetService.save(reTweet);

        return "redirect:/profile/"+principal.getName()+".html";
    }

    @RequestMapping(method = RequestMethod.POST, params = {"!tweetID"})
    public String addTweet(@RequestParam String text, @RequestParam String geo, @RequestParam String status, Principal principal) {
        Tweet tweet = new Tweet();
        tweet.setText(text);
        tweet.setCoords(geo);
        tweet.setPublishedDate(new Date());
        tweet.setUser(userService.findOneByNameTweet(principal.getName()));
        tweet.setStatus(statusService.findOneByName(status));
        tweetService.save(tweet);
        if (tweet.getStatus().getId() == 2)
            return "redirect:" + "/tweet/" + tweet.getId() + "/edit.html";
        else
            return "redirect:/profile/"+principal.getName()+".html";
    }

    @RequestMapping("/tweet/remove/{id}")
    public String removeTweet(@PathVariable int id, Principal principal) {
        tweetService.delete(tweetService.findOneTweet(id));
        return "redirect:/profile/"+principal.getName()+".html";
    }

    @RequestMapping("/retweet/remove/{id}")
    public String removeReTweet(@PathVariable int id, Principal principal) {
        reTweetService.delete(reTweetService.findOne(id));
        return "redirect:/profile/"+principal.getName()+".html";
    }
}
