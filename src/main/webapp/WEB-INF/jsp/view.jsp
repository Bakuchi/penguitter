<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../layout/taglib.jsp" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 08.04.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">

<hr>
<div class="t_body">
    <div class="tweet">
        <h2><c:out value="Твит: ${tweet.text}"/></h2>

        <h2><c:out value="Гео: ${tweet.coords}"/></h2>
    </div>
</div>
