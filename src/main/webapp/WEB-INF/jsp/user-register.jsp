<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">
<%@ include file="../layout/taglib.jsp" %>

<form:form commandName="user" cssClass="form-horizontal registrationForm">

	<c:if test="${param.success eq true}">
		<div class="alert alert-success">Registration successfull!</div>
	</c:if>
	<div class="font_classic">
	<div class="form-group">

		<label for="name" class="col-sm-2 control-label">Name:</label>

		<div class="col-sm-10">
			<div class="font_panton">
			<form:input path="name" cssClass="form-control" />
			<form:errors path="name" />
				</div>
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">Email:</label>
		<div class="col-sm-10">
			<div class="font_panton">
			<form:input path="email" cssClass="form-control" />
			<form:errors path="email" />
				</div>
		</div>
	</div>
	<div class="form-group">
		<label for="password" class="col-sm-2 control-label">Password:</label>
		<div class="col-sm-10">
			<div class="font_panton">
			<form:password path="password" cssClass="form-control" />
			<form:errors path="password" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="password_again" class="col-sm-2 control-label">Password again:</label>
		<div class="col-sm-10">
			<div class="font_panton">
			<input type="password" name="password_again" id="password_again" class="form-control" />
				</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-2">
			<input type="submit" value="Save" class="btn btn-lg btn-primary" />

		</div>
	</div>
	</div>
</form:form>


<script type="text/javascript">
	jQuery.validator.addMethod(
			'regexp',
			function(value, element, regexp) {
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			},
			"Please check your input."
	);

	$(document).ready(function() {

	$(".registrationForm").validate(
		{
			rules: {
				name: {
					required : true,
					minlength : 3,
					maxlength : 15,
					regexp: '^[a-zA-Z0-9_]+$',
					remote : {
						url: "<spring:url value='/register/available.html' />",
						type: "get",
						data: {
							username: function() {
								return $("#name").val();
							}
						}
					}
				},
				email: {
					required : true,
					email: true
				},
				password: {
					required : true,
					regexp: '^[a-z0-9_-]{5,16}$',
					minlength : 5
				},
				password_again: {
					required : true,
					minlength : 5,
					equalTo: "#password"
				}
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			messages: {
				name: {
					remote: "Such username already exists!",
					minlength: "Введите больше 3 симолов",
					maxlength: "Введите меньше 15 символов",
					regexp: "Логин должен состоять из обычных или заглавных латинских символов и цифр"
				}
			}
		}
	);
});
</script>