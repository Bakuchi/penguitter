<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp"%>
<style>
  .t_body{
    width: 1000px;
  }
  .tweet{
    width: 500px;
    float: left;
  }
  .retweet{
    float: right;
    margin-left: 50px;
    width: 450px;
    /*border-right: solid beige 1px;*/
  }
  h2{
    font-family: Verdana;
  }
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $('.nav-tabs a:first').tab('show'); // Select first tab
    $(".triggerRemove").click(function(e) {
      e.preventDefault();
      $("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
      $("#modalRemove").modal();
    });
  });
</script>

<h1><c:out value="@${user.name}" /></h1>
<!-- Tab panes -->
<div class="t_body">
<div class="tweet">
  <h2>Твиты:</h2>
<c:forEach items="${user.tweets}" var="tweet">
  <div id="tweet_${tweet.id}">
    <a href="/account.html"><c:out value="@${user.name}" /></a><br/>
    <c:out value="${tweet.text}" /><br/>
    <a id="right_menu" href="<spring:url value="/tweet/remove/${tweet.id}.html" />" class="btn btn-danger triggerRemove"><img height="22px" width="22px" src="${pageContext.request.contextPath}/layout/img/delete.png"/></a>
    <a id="right_menu" href="<spring:url value="/tweet/edit/${tweet.id}.html" />" class="btn btn-danger triggerEdit"><img src="${pageContext.request.contextPath}/layout/img/edit.png"/></a>

    <a id="right_menu" href="<spring:url value="/tweet/retweet/${tweet.id}.html" />" class="btn btn-danger triggerRemove"><img height="22px" width="22px" src="${pageContext.request.contextPath}/layout/img/retweet.png"/></a>
    <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_${tweet.id}">
      New tweet
    </button>

    <form:form commandName="user" cssClass="form-horizontal tweetForm">
      <!-- Modal -->
      <div class="modal fade" id="myModal_${reTweet.id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">New tweet:</h4>
            </div>
            <div class="modal-body">

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Text:</label>
                <div class="col-sm-10">
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" value="Send" />
            </div>
          </div>
        </div>
      </div>
    </form:form>
    <h6><i><c:out value="Опубликовано ${reTweet.publishedDate}" /></i></h6>
  </div>
  <hr>
</c:forEach>
  </div>
<div class="retweet">
  <h2>Ретвиты:</h2>
  <c:forEach items="${user.reTweets}" var="tweet">
    <div id="tweet_${tweet.id}">
      <a href="/account.html"><c:out value="@${user.name}" /></a><br/>
      <c:out value="${tweet.text}" /><br/>
      <a id="right_menu" href="<spring:url value="/tweet/remove/${tweet.id}.html" />" class="btn btn-danger triggerRemove"><img height="22px" width="22px" src="${pageContext.request.contextPath}/layout/img/delete.png"/></a>
      <a id="right_menu" href="<spring:url value="/tweet/edit/${tweet.id}.html" />" class="btn btn-danger triggerEdit"><img src="${pageContext.request.contextPath}/layout/img/edit.png"/></a>

      <a id="right_menu" href="<spring:url value="/tweet/retweet/${tweet.id}.html" />" class="btn btn-danger triggerRemove"><img height="22px" width="22px" src="${pageContext.request.contextPath}/layout/img/retweet.png"/></a>


      <h6><i><c:out value="Опубликовано ${tweet.publishedDate}" /></i></h6>
      <h6><i><c:out value="Ретвит от пользователя @${tweet.tweet.user.name}"/></i></h6>
    </div>
    <hr>
  </c:forEach>
</div>
  </div>
<hr/>
<br /><br />

<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Remove tweet</h4>
      </div>
      <div class="modal-body">
        Really remove?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="" class="btn btn-danger removeBtn">Remove</a>
      </div>
    </div>
  </div>
</div>