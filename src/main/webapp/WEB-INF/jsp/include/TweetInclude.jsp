<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 12.04.2015
  Time: 9:47
  To change this template use File | Settings | File Templates.
--%>

<script type="text/javascript">
    $(document).ready(function () {
        $(".triggerRemove").click(function (e) {
            e.preventDefault();
            $("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
            $("#modalRemove").modal();
        });
    });
</script>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="tweet">
    <h2><c:out value="Твиты:"/></h2>
    <c:forEach items="${user.tweets}" var="tweet">
        <security:authorize
                access="hasRole('ROLE_USER') and not (hasRole('ROLE_ADMIN') or principal.username=='${user.name}')">
            <c:choose>
                <c:when test="${tweet.status.id==1}">

                    <div id="tweet_${tweet.id}">
                        <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a><br/>
                        <c:out value="${tweet.text}"/><br/>

                        <h6><i>
                            <c:choose>
                                <c:when test="${tweet.days_ago<7}">
                                    <c:out value="Опубликовано ${tweet.publishedDate}"/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="Опубликовано ${tweet.days_ago} дней назад"/>
                                </c:otherwise>
                            </c:choose>
                        </i></h6>

                        <security:authorize access="isAuthenticated() and principal.username!='${user.name}'">
                            <a class="btn btn-lg" data-toggle="modal" data-target="#myModalR_${tweet.id}">
                                <span class=" glyphicon glyphicon-refresh"/>
                            </a>
                        </security:authorize>


                        <form method="post" cssClass="form-horizontal tweetForm">

                            <div class="modal fade" id="myModalR_${tweet.id}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">RTweet:</h4>
                                        </div>
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Text:</label>

                                                <div class="col-sm-10">
                                                    <input type="hidden" id="tID" name="tweetID" value="${tweet.id}">
                                                    <input pattern=".{1,150}" required title="Символов должно быть от 0 до 150." type="text" id="text"
                                                           name="text"/>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <input type="submit" class="btn btn-primary" value="Send"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr>
                </c:when>
                <c:when test="${tweet.status.id==2}">

                    <c:forEach items="${tweet.usersWithAccess}" var="access">
                        <security:authorize access="principal.username=='${access.name}'">
                            <div id="tweet_${tweet.id}">
                                <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a><br/>
                                <c:out value="${tweet.text}"/><br/>

                                <h6><i> <c:choose> <c:when test="${tweet.days_ago<7}"> <c:out
                                        value="Опубликовано ${tweet.publishedDate}"/> </c:when> <c:otherwise> <c:out
                                        value="Опубликовано ${tweet.days_ago} дней назад"/> </c:otherwise>
                                </c:choose> </i></h6>
                            </div>
                            <hr>
                        </security:authorize>

                    </c:forEach>

                </c:when>
            </c:choose>
        </security:authorize>
        <security:authorize access="isAuthenticated()">
        <security:authorize access="hasRole('ROLE_ADMIN') or principal.username=='${user.name}'">
            <div id="tweet_${tweet.id}">
                <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a><br/>
                <c:out value="${tweet.text}"/><br/>

                <h6><i> <c:choose> <c:when test="${tweet.days_ago<7}"> <c:out
                        value="Опубликовано ${tweet.publishedDate}"/> </c:when> <c:otherwise> <c:out
                        value="Опубликовано ${tweet.days_ago} дней назад"/> </c:otherwise> </c:choose> </i></h6>

                <security:authorize access="isAuthenticated() and principal.username=='${user.name}'">
                    <c:if test="${tweet.status.name.equals(\"STATUS_CLOSED\")}">
                    <a id="right_menu" class="btn  btn-lg"
                       href="<spring:url value="/tweet/${tweet.id}/edit.html" />">
                        <span class="glyphicon glyphicon-edit"/>
                    </a>
                    </c:if>
                    <a id="right_menu" class="btn  btn-lg"
                       href="<spring:url value="/tweet/${tweet.id}/edit_tweet.html" />">
                        <span class="glyphicon glyphicon-pencil"/>
                    </a>
                    <a id="right_menu" class="btn  btn-lg triggerRemove"
                       href="<spring:url value="/profile/${tweet.user.name}/tweet/remove/${tweet.id}.html" />">
                        <span class="glyphicon glyphicon-trash"/>
                    </a>
                </security:authorize>
                <security:authorize access="isAuthenticated() and principal.username!='${user.name}'">
                    <a class="btn btn-lg" data-toggle="modal" data-target="#myModalR_${tweet.id}">
                        <span class=" glyphicon glyphicon-refresh"/>
                    </a>
                </security:authorize>


                <form method="post" cssClass="form-horizontal tweetForm">

                    <div class="modal fade" id="myModalR_${tweet.id}" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">RTweet:</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Text:</label>

                                        <div class="col-sm-10">
                                            <input type="hidden" id="tID" name="tweetID" value="${tweet.id}">
                                            <input pattern=".{1,150}" required title="Символов должно быть от 0 до 150." type="text" id="text" name="text"/>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                    <input type="submit" class="btn btn-primary" value="Send"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
        </security:authorize>
        </security:authorize>

        <security:authorize access="not hasRole('ROLE_USER') and not hasRole('ROLE_ADMIN')">
            <c:if test="${tweet.status.id==1}">
                <div id="tweet_${tweet.id}">
                    <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a><br/>
                    <c:out value="${tweet.text}"/><br/>

                    <h6><i> <c:choose> <c:when test="${tweet.days_ago<7}"> <c:out
                            value="Опубликовано ${tweet.publishedDate}"/> </c:when> <c:otherwise> <c:out
                            value="Опубликовано ${tweet.days_ago} дней назад"/> </c:otherwise> </c:choose> </i></h6>
                </div>
                <hr>
            </c:if>
        </security:authorize>
    </c:forEach>
</div>

<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Remove tweet</h4>
            </div>
            <div class="modal-body">
                Really remove?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="" class="btn btn-danger removeBtn">Remove</a>
            </div>
        </div>
    </div>
</div>