<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 12.04.2015
  Time: 9:47
  To change this template use File | Settings | File Templates.
--%>
<style>
    .tweet_t {
        padding-left: 3%;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".trigger").click(function (e) {
            e.preventDefault();
            $("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
            $("#modalRemove").modal();
        });
    });
</script>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="retweet">
    <h2><c:out value="Ретвиты:"/></h2>
    <c:forEach items="${user.reTweets}" var="retweet">
    <security:authorize
            access="hasRole('ROLE_USER') and not (hasRole('ROLE_ADMIN') or principal.username=='${user.name}')">
    <c:choose>
    <c:when test="${retweet.status.id==1}">
    <div id="tweet_${retweet.id}">
        <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a> retweeted<br/>
        <c:out value="${retweet.text}"/><br/>

        <div class="tweet_t">
            <span class="glyphicon glyphicon-retweet" style="color: limegreen"></span><a
                href="/profile/${retweet.tweet.user.name}.html"><c:out value="@${retweet.tweet.user.name}"/></a><br/>
            <c:out value="${retweet.tweet.text}"/><br/>
        </div>

        <h6><i> <c:choose> <c:when test="${retweet.days_ago<7}"> <c:out value="Опубликовано ${retweet.publishedDate}"/>
        </c:when> <c:otherwise> <c:out value="Опубликовано ${retweet.days_ago} дней назад"/> </c:otherwise>
        </c:choose> </i></h6>

        <security:authorize access="isAuthenticated() and principal.username=='${user.name}'">
            <c:if test="${retweet.status.name.equals(\"STATUS_CLOSED\")}">
                <a id="right_menu" class="button btn-lg"
                   href="<spring:url value="/retweet/${retweet.id}/edit_access.html" />">
                    <span class="glyphicon glyphicon-edit"/>
                </a>
            </c:if>
            <a id="right_menu" class="button btn-lg"
               href="<spring:url value="/retweet/${retweet.id}/edit_retweet.html" />">
                <span class="glyphicon glyphicon-pencil"/>
            </a>
            <a id="right_menu" class="button btn-lg trigger"
               href="<spring:url value="/profile/${retweet.user.name}/retweet/remove/${retweet.id}.html" />">
                <span class="glyphicon glyphicon-trash"/>
            </a>
        </security:authorize>
    </div>
    <hr>
    </c:when>
    <c:when test="${retweet.status.id==2}">

    <c:forEach items="${retweet.usersWithAccess}" var="access">
    <security:authorize access="principal.username=='${access.name}'">

    <div id="tweet_${retweet.id}">
        <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a> retweeted<br/>
        <c:out value="${retweet.text}"/><br/>

        <div class="tweet_t">
            <span class="glyphicon glyphicon-retweet" style="color: limegreen"></span>
            <a href="/profile/${retweet.tweet.user.name}.html"><c:out value="@${retweet.tweet.user.name}"/></a><br/>
            <c:out value="${retweet.tweet.text}"/><br/>
        </div>

        <h6><i> <c:choose> <c:when test="${retweet.days_ago<7}"> <c:out value="Опубликовано ${retweet.publishedDate}"/>
        </c:when> <c:otherwise> <c:out value="Опубликовано ${retweet.days_ago} дней назад"/> </c:otherwise>
        </c:choose> </i></h6>
        <security:authorize access="isAuthenticated() and principal.username=='${user.name}'">
            <c:if test="${retweet.status.name.equals(\"STATUS_CLOSED\")}">
                <a id="right_menu" class="button btn-lg"
                   href="<spring:url value="/retweet/${retweet.id}/edit_access.html" />">
                    <span class="glyphicon glyphicon-edit"/>
                </a>
            </c:if>
            <a id="right_menu" class="button btn-lg"
               href="<spring:url value="/retweet/${retweet.id}/edit_retweet.html" />">
                <span class="glyphicon glyphicon-pencil"/>
            </a>
            <a id="right_menu" class="button btn-lg trigger"
               href="<spring:url value="/profile/${retweet.user.name}/retweet/remove/${retweet.id}.html" />">
                <span class="glyphicon glyphicon-trash"/>
            </a>
        </security:authorize>
    </div>
    <hr>

    </security:authorize>

    </c:forEach>

    </c:when>
    </c:choose>
    </security:authorize>
    <security:authorize access="isAuthenticated()">
    <security:authorize access="hasRole('ROLE_ADMIN') or principal.username=='${user.name}'">
    <div id="tweet_${retweet.id}">
        <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a> retweeted<br/>
        <c:out value="${retweet.text}"/><br/>

        <div class="tweet_t">
            <span class="glyphicon glyphicon-retweet" style="color: limegreen"></span>
            <a href="/profile/${retweet.tweet.user.name}.html"><c:out value="@${retweet.tweet.user.name}"/></a><br/>
            <c:out value="${retweet.tweet.text}"/><br/>
        </div>

        <h6><i> <c:choose> <c:when test="${retweet.days_ago<7}"> <c:out value="Опубликовано ${retweet.publishedDate}"/>
        </c:when> <c:otherwise> <c:out value="Опубликовано ${retweet.days_ago} дней назад"/> </c:otherwise>
        </c:choose> </i></h6>
        <security:authorize access="isAuthenticated() and principal.username=='${user.name}'">
            <c:if test="${retweet.status.name.equals(\"STATUS_CLOSED\")}">
                <a id="right_menu" class="button btn-lg"
                   href="<spring:url value="/retweet/${retweet.id}/edit_access.html" />">
                    <span class="glyphicon glyphicon-edit"/>
                </a>
            </c:if>
            <a id="right_menu" class="button btn-lg"
               href="<spring:url value="/retweet/${retweet.id}/edit_retweet.html" />">
                <span class="glyphicon glyphicon-pencil"/>
            </a>
            <a id="right_menu" class="button btn-lg trigger"
               href="<spring:url value="/profile/${retweet.user.name}/retweet/remove/${retweet.id}.html" />">
                <span class="glyphicon glyphicon-trash"/>
            </a>
        </security:authorize>
    </div>
    <hr>
    </security:authorize>
    </security:authorize>
    <security:authorize access="not hasRole('ROLE_ADMIN') and not hasRole('ROLE_USER')">
    <c:if test="${retweet.status.id==1}">
    <div id="tweet_${retweet.id}">
        <a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a> retweeted<br/>
        <c:out value="${retweet.text}"/><br/>

        <div class="tweet_t">
            <span class="glyphicon glyphicon-retweet" style="color: limegreen"></span>
            <a href="/profile/${retweet.tweet.user.name}.html"><c:out value="@${retweet.tweet.user.name}"/></a><br/>
            <c:out value="${retweet.tweet.text}"/><br/>
        </div>

        <h6><i> <c:choose> <c:when test="${retweet.days_ago<7}"> <c:out value="Опубликовано ${retweet.publishedDate}"/>
        </c:when> <c:otherwise> <c:out value="Опубликовано ${retweet.days_ago} дней назад"/> </c:otherwise>
        </c:choose> </i></h6>
    </div>
    <hr>
    </c:if>
    </security:authorize>

        <%--</security:accesscontrollist>--%>
    </c:forEach>

    <!-- Modal -->
    <div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Remove retweet</h4>
                </div>
                <div class="modal-body">
                    Really remove?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="" class="btn btn-danger removeBtn">Remove</a>
                </div>
            </div>
        </div>
    </div>
