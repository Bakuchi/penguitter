<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../layout/taglib.jsp" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 08.04.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<style>
    .added_users {
        width: 250px;
    }

    a.button15 {
        display: inline-block;
        font-family: arial, sans-serif;
        font-size: 11px;
        font-weight: bold;
        color: rgb(68, 68, 68);
        text-decoration: none;
        user-select: none;
        padding: .2em 1.2em;
        outline: none;
        border: 1px solid rgba(0, 0, 0, .1);
        border-radius: 2px;
        background: rgb(245, 245, 245) linear-gradient(#f4f4f4, #f1f1f1);
        transition: all .218s ease 0s;
    }

    a.button15:hover {
        color: rgb(24, 24, 24);
        border: 1px solid rgb(198, 198, 198);
        background: #f7f7f7 linear-gradient(#f7f7f7, #f1f1f1);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .1);
    }

    a.button15:active {
        color: rgb(51, 51, 51);
        border: 1px solid rgb(204, 204, 204);
        background: rgb(238, 238, 238) linear-gradient(rgb(238, 238, 238), rgb(224, 224, 224));
        box-shadow: 0 1px 2px rgba(0, 0, 0, .1) inset;
    }

    hr.after_add {
        width: 250px;
    }
</style>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">


<hr>
<security:authorize access="isAuthenticated() and principal.username=='${tweet.user.name}'">

    <div class="t_body">
        <div class="tweet">
            <h2><c:out value="Редактирование твита..."/></h2>

            <form method="post">
                    <c:out value="Текст твита"/><br/>
                        <input pattern=".{1,150}" required type="text" name="text" value="${tweet.text}"/>
                <br/>
                    <c:out value="Координаты"/><br/>
                <input type="text" name="coords" value="${tweet.coords}"/>
                <br/>
                    <c:out value="Настройка доступа твита"/>
                <div class="select-type-tweet">
                    <select id="status" name="status">
                        <option value="${tweet.status.getName()}">
                        <c:if test="${tweet.status.getName() == 'STATUS_CLOSED'}">Защищенный твит</option>
                        <option name="status" value="STATUS_OPEN">Открытый твит</option>
                        <option name="status" value="STATUS_SECRET">Секретный твит</option></c:if>
                            <c:if test="${tweet.status.getName() == 'STATUS_OPEN'}">Открыый твит</option>
                                <option name="status" value="STATUS_CLOSED">Защищенный твит</option>
                                <option name="status" value="STATUS_SECRET">Секретный твит</option></c:if>
                            <c:if test="${tweet.status.getName() == 'STATUS_SECRET'}">Секретный твит</option>
                                <option name="status" value="STATUS_OPEN">Открытый твит</option>
                                <option name="status" value="STATUS_CLOSED">Защищенный твит</option></c:if>
                    </select>
                </div>
        </div>
        <input type="submit"/>
        </form>
        <br/><br/>
    </div>
    </div>
</security:authorize>