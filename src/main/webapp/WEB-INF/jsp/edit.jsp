<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../layout/taglib.jsp" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 08.04.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<style>
    .added_users {
        width: 250px;
    }

    a.button15 {
        display: inline-block;
        font-family: arial, sans-serif;
        font-size: 11px;
        font-weight: bold;
        color: rgb(68, 68, 68);
        text-decoration: none;
        user-select: none;
        padding: .2em 1.2em;
        outline: none;
        border: 1px solid rgba(0, 0, 0, .1);
        border-radius: 2px;
        background: rgb(245, 245, 245) linear-gradient(#f4f4f4, #f1f1f1);
        transition: all .218s ease 0s;
    }

    a.button15:hover {
        color: rgb(24, 24, 24);
        border: 1px solid rgb(198, 198, 198);
        background: #f7f7f7 linear-gradient(#f7f7f7, #f1f1f1);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .1);
    }

    a.button15:active {
        color: rgb(51, 51, 51);
        border: 1px solid rgb(204, 204, 204);
        background: rgb(238, 238, 238) linear-gradient(rgb(238, 238, 238), rgb(224, 224, 224));
        box-shadow: 0 1px 2px rgba(0, 0, 0, .1) inset;
    }

    hr.after_add {
        width: 250px;
        float: left;
    }
</style>

<script>
    var prefix = '/api/tweets/';
    var RestDelete = function (sd) {
        var sdsa =
                {
                    user_name: sd
                }
                ;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: prefix + "delete/" +${tweet.id},
            data: JSON.stringify(sdsa),
            processData: false,
            success: function (data, textStatus) {
                console.log(data);
                console.log(textStatus);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    };
    var deleteUser = function (s) {
        console.log(s);
        var users = document.getElementsByClassName("button15");
        var added_users = document.getElementsByClassName("added_users")[0].innerHTML;
        var added_users2 = document.getElementsByClassName("added_users")[1].innerHTML;
        console.log(100 < users.length);
        for (var i = 0; i < users.length; i++) {
            console.log('=====' + i);
            console.log('true or false =' + users[i].innerHTML == s);
            if (users[i].innerHTML == s) {
                RestDelete(s);
                console.log('Hey');
            } else {
                added_users2 += '' +
                        '<a href="#" class="button15" onclick="deleteUser(\'' + users[i].innerHTML + '\')">' +
                        '' + users[i].innerHTML + '</a>&nbsp;';
            }
        }
        document.getElementsByClassName("added_users")[0].innerHTML = added_users2;
        console.log(added_users2);
    };
    var RestPost = function (sd) {
        var sdsa =
                {
                    user_name: sd
                }
                ;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: prefix + "add/" +${tweet.id},
            data: JSON.stringify(sdsa),
            processData: false,
            success: function (data, textStatus) {
                console.log(data);
//alert("success");
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
//alert('request failed'+errorThrown);
            }
        });
    };
    var RestGet = function () {
        var f = true;
        var f1 = true;
        $.ajax({
            type: 'GET',
            url: "/api/users/" + document.getElementById("input_username").value,
            dataType: 'json',
            async: true,
            success: function (result) {
                RestPost(document.getElementById("input_username").value);
                if (document.getElementsByClassName("button15").length == 0) {
                    document.getElementsByClassName("added_users")[0].innerHTML += '' +
                            '<a href="#" class="button15" onclick="deleteUser(\'' + document.getElementById("input_username").value + '\')">' + document.getElementById("input_username").value +
                            '</a>&nbsp;';
                    f = false;
                    f1 = false;
                }
                if (f) {
                    for (var i = 0; i < document.getElementsByClassName("button15").length; i++) {
                        if (!(document.getElementsByClassName("button15")[i].innerHTML == document.getElementById("input_username").value)) {

                        }
                        else {
                            if (f) {
                                f1 = f1 & false;
                                alert("Такой пользователь уже добавлен в список!")
                            }
                        }
                    }
                }
                if (f1 == true) {
                    document.getElementsByClassName("added_users")[0].innerHTML += '' +
                            '<a href="#" class="button15" onclick="deleteUser(\'' + document.getElementById("input_username").value + '\')">' + document.getElementById("input_username").value +
                            '</a>&nbsp;';
                }
            },
            error: function () {
                alert("Нет такого пользователя!");
            }
        });
    };
</script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">

<hr>
<security:authorize access="isAuthenticated() and principal.username=='${tweet.user.name}'">

    <div class="t_body">
        <div class="tweet">
            <h2><c:out value="Твит: ${tweet.text}"/></h2>

            <div class="select-type-tweet">
                <select id="status" name="status">
                    <option selected="selected" name="status" value="STATUS_CLOSED">Защищенный твит</option>
                </select>
            </div>
            <br/><br/>
            Введите имя пользователя:
            <input type="text" id="input_username" placeholder="SomeName"/>
            <button type="button" onclick="RestGet()">Add</button>
            <br/>
            <hr class="after_add">

            <div class="added_users"><c:forEach items="${tweet.usersWithAccess}" var="users">
                <a href="#" class="button15" onclick="deleteUser('${users.name}')">${users.name}</a>
            </c:forEach></div>
            <div class="added_users"></div>
        </div>
    </div>
</security:authorize>