<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<style>
    .tweets{
        width: 600px;
        margin-left: 20%;
        padding-left: 10%;
        padding-right: 10%;
        padding-top: 2%;
        word-break: break-all;
    }
</style>
<%@ include file="../layout/taglib.jsp"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">
<center><h2>Последние 10 твитов пользователей:</h2></center>
<hr>
<div class="tweets">
    <c:forEach items="${tweets}" var="tweet">
        <c:choose>
            <c:when test="${tweet.status.id==1}">
                <div id="tweet_${tweet.id}">
                    <a href=<spring:url value="/profile/${tweet.user.name}.html"/>><c:out value="@${tweet.user.name}" /></a><br/>
                    <div class="text">
                        <c:out value="${tweet.text}" /><br/></div>
                    <h6><i><c:out value="Опубликовано ${tweet.publishedDate}" /></i></h6>
                </div>
                <hr>
            </c:when>
        </c:choose>
    </c:forEach>
</div>


