<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../layout/taglib.jsp" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 08.04.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">

<hr>
<security:authorize access="isAuthenticated() and principal.username=='${retweet.user.name}'">

    <div class="t_body">
        <div class="tweet">
            <h2><c:out value="Редактирование твита..."/></h2>

            <form method="post">
                    <c:out value="Текст ретвита"/><br/>
                        <input pattern=".{1,150}" required type="text" name="text" value="${retweet.text}"/>
                <br/>
                <br/>
                    <c:out value="Настройка доступа ретвита"/>
                <div class="select-type-tweet">
                    <select id="status" name="status">
                        <option value="${retweet.status.getName()}">
                            <c:if test="${retweet.status.getName() == 'STATUS_CLOSED'}">Защищенный твит</option>
                        <option name="status" value="STATUS_OPEN">Открытый твит</option>
                        <option name="status" value="STATUS_SECRET">Секретный твит</option></c:if>
                        <c:if test="${retweet.status.getName() == 'STATUS_OPEN'}">Открытый твит</option>
                            <option name="status" value="STATUS_CLOSED">Защищенный твит</option>
                            <option name="status" value="STATUS_SECRET">Секретный твит</option></c:if>
                        <c:if test="${retweet.status.getName() == 'STATUS_SECRET'}">Секретный твит</option>
                            <option name="status" value="STATUS_OPEN">Открытый твит</option>
                            <option name="status" value="STATUS_CLOSED">Защищенный твит</option></c:if>
                    </select>
                </div>
        </div>
        <input type="submit"/>
        </form>
    </div>
    </div>
</security:authorize>