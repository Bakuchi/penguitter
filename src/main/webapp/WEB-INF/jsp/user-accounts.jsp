<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/layout/js/jquery-2.1.3.min.js"></script>

<script>
    function addInputs(obj){
        var divo = document.getElementById('select-type-tweet-private');
        var description = document.getElementById('description');
        if (obj.selectedIndex) {
            if (obj.value=="STATUS_CLOSED") {
                description.innerHTML = '<h6>*Защищенные твиты видны тем, кто имеет к ним доступ. Введите ники тех пользователей, которым вы дадите доступ к этому твиту.</h6>';
                divo.innerHTML = 'Никнейм: <input type="text"/>';
            } else if (obj.value == "STATUS_OPEN"){
                divo.innerHTML = '';
                description.innerHTML = '<h6>*Открытые твиты видят все.</h6>';
            } else if (obj.value == "STATUS_SECRET") {
                divo.innerHTML = '';
                description.innerHTML = '<h6>*Секретные твиты видны только автору.</h6>';
            }
        }
    }
</script>
<style type="text/css">
    #right_menu{
        float: right;
    }
    .select-type-tweet{

    }
    .select-type-tweet select{
        float: left;
        width: 140px;
        border-radius: 10px;
        /*for WebKit*/
        -webkit-appearance: none;
        /* for FF */
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
        /* for IE */
        -ms-appearance: none;
        appearance: none!important;
    }
    .select-type-tweet option{
        text-align: center;
    }
    #select-type-tweet-private{
        float: right;
    }
    #select-type-tweet-private input{
        border-radius: 10px;
    }
    #description {
        width: 200px;
    }

</style>
<!-- Button trigger modal -->
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    New tweet
</button>


<form method="post" cssClass="form-horizontal tweetForm">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Tweet:</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Text:</label>
                        <div class="col-sm-10">
                            <input type="text" id="text" name="text"/>

                            <hr>

                            <div class="select-type-tweet">
                                <select path="" onchange="addInputs(this)">
                                <option value="">Выберите тип...</option>
                                <option value="STATUS_OPEN">Открытый твит</option>
                                <option value="STATUS_CLOSED">Защищенный твит</option>
                                <option value="STATUS_SECRET">Секретный твит</option>
                                </select>
                            </div>
                            <div id="select-type-tweet-private">

                            </div>
                            <br/><br/>
                            <div id="description">

                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</form>

<br /><br />

<script type="text/javascript">
    $(document).ready(function() {
        $('.nav-tabs a:first').tab('show'); // Select first tab
        $(".triggerRemove").click(function(e) {
            e.preventDefault();
            $("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
            $("#modalRemove").modal();
        });
        $(".tweetForm").validate(
                {
                    rules: {
                        text: {
                            required : true,
                            maxCharacters: 150
                        }
                    },
                    highlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                }
        );
    });
</script>

<!-- Tab panes -->
<c:forEach items="${user.tweets}" var="tweet">
    <div id="tweet_${tweet.id}">
        <c:out value="@${user.name}" /><br/>
        <c:out value="${tweet.text}" /><br/>
        <a id="right_menu" class="button btn-lg"  href="<spring:url value="/tweet/edit/${tweet.id}.html" />">
            <span class=" glyphicon glyphicon-pencil" data-toggle="modal" data-target="#myModal_${tweet.id}" />
        </a>
        <a id="right_menu" class="button btn-lg"  href="<spring:url value="/tweet/remove/${tweet.id}.html" />">
            <span class=" glyphicon glyphicon-trash" data-toggle="modal" data-target="#myModal_${tweet.id}" />
        </a>
        <h6><i><c:out value="Опубликовано ${tweet.publishedDate}" /></i></h6>
    </div>
    <hr>
</c:forEach>


<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Remove tweet</h4>
            </div>
            <div class="modal-body">
                Really remove?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="" class="btn btn-danger removeBtn">Remove</a>

            </div>
        </div>
    </div>
</div>