<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../layout/taglib.jsp"%>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 08.04.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<style>
    .t_body{
        width: 1000px;
    }
    .tweet{
        width: 500px;
        float: left;
        padding-left: 30px;
        border-radius: 5px;
    }
    .retweet{
        float: right;
        margin-left: 50px;
        width: 450px;
        padding-left: 30px;
        border-radius: 5px;
    }
    h2{
        font-family: Verdana;
    }
    .select-type-tweet select{
        float: left;
        width: 140px;
        border-radius: 10px;
        /*for WebKit*/
        -webkit-appearance: none;
        /* for FF */
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
        /* for IE */
        -ms-appearance: none;
        appearance: none!important;
    }
    .select-type-tweet option{
        text-align: center;
    }
    #select-type-tweet-private{
        float: right;
    }
    #select-type-tweet-private input{
        border-radius: 10px;
    }
    #description {
        width: 200px;
    }
    .added_users{
        margin-left: 68px;
    }
    .user{
        border-radius: 10px;
        border: solid chartreuse 1px;
        margin-right: 40px;
    }
    .t_login {
        width: 1140px;
    }
    #new_tweet_button{
        float: left;
        margin-top: -50px;
    }
    .tweetInclude{
        width: 450px;
    }
</style>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/style.css">
<script>
    function addInputs(obj){
        var divo = document.getElementById('select-type-tweet-private');
        var description = document.getElementById('description');
        if (obj.selectedIndex) {
            if (obj.value=="STATUS_CLOSED") {
                description.innerHTML = '<h6>*Защищенные твиты видны тем, кто имеет к ним доступ. Введите ники тех пользователей, которым вы дадите доступ к этому твиту.</h6>';
            } else if (obj.value == "STATUS_OPEN"){
                divo.innerHTML = '';
                description.innerHTML = '<h6>*Открытые твиты видят все.</h6>';
            } else if (obj.value == "STATUS_SECRET") {
                divo.innerHTML = '';
                description.innerHTML = '<h6>*Секретные твиты видны только автору.</h6>';
            }
        }
        else {
            divo.innerHTML = '';
            description.innerHTML = '';

        }
    }
</script>
<div class="t_login">
    <center><h2><a href="/profile/${user.name}.html"><c:out value="@${user.name}"/></a></h2></center>
    <security:authorize access="isAuthenticated() and principal.username=='${user.name}'">
        <button id="new_tweet_button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
            New tweet
        </button>
    </security:authorize>
</div>
<hr>
<div class="t_body">

    <div class="tweetInclude" style="word-break: break-all"><jsp:include page="include/TweetInclude.jsp">
        <jsp:param name="user" value="${user}"/>
    </jsp:include></div>

    <div class="retweetInclude" style="word-break: break-all"><jsp:include page="include/RetweetInclude.jsp">
        <jsp:param name="user" value="${user}"/>
    </jsp:include></div>


    <form method="post" cssClass="form-horizontal tweetForm">

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Tweet:</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label">Text:</label>
                            <input pattern=".{1,150}" required title="Символов должно быть от 0 до 150." type="text" id="text" name="text"/>
                            <label class=" control-label">Geo:</label>
                            <input type="text" id="geo" name="geo"/>
                            <div class="col-sm-10">
                                <hr>
                                <div class="select-type-tweet">
                                    <select id = "status" name="status" onchange="addInputs(this)">
                                        <option name="status" value="STATUS_OPEN" >Открытый твит</option>
                                        <option name="status" value="STATUS_CLOSED">Защищенный твит</option>
                                        <option name="status" value="STATUS_SECRET">Секретный твит</option>
                                    </select>
                                </div>
                                <div id="select-type-tweet-private">

                                </div>
                                <br/><br/>
                                <div id="description">

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Remove tweet</h4>
                </div>
                <div class="modal-body">
                    Really remove?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="" class="btn btn-danger removeBtn">Remove</a>

                </div>
            </div>
        </div>
    </div>
</div>

